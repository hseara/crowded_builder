#!/usr/bin/env python3

#Created by Leija 11.6.2014

from optparse import OptionParser
import math
#from decimal import *
#getcontext().prec = 10

W_VOLUME = 1342.9 / 10000

def optP():
    usage="[python3] %prog -f .dat -c .dat -i .inp -o .pdb -p .top"
    description="Creates inputfiles to packmol and vmd. also topology"
    version="\n%prog Version 0.1\n\nRequires Python 3.0 or newer."
    optParser = OptionParser(usage=usage, description=description, version=version)
    optParser.set_defaults(inputfile='system_input1.dat', forcefield='martini22', reducedto=40, datainput='cg_protein_db.dat', multiply=3)
    optParser.add_option('-f', type='str', dest='inputfile',
                         help="inputfile consisting system parameters [default: %default]")
    optParser.add_option('-i', type='str', dest='forcefield',
                         help="forcefield [martini22|martini21] [default: %default]")
    optParser.add_option('-p', type='int', dest='reducedto',
                         help="include forcefield where epsilon reduced to  [default: %default]")
    optParser.add_option('-c', type='str', dest='datainput',
                         help="database input [default: %default]")
    optParser.add_option('-v', type='float', dest='multiply', help="volume multiplied by [default: %default]")
    optParser.add_option('-s', '--sort', action="store_true", dest="verbose", default=False, help="colour by protein name")
    return optParser.parse_args()


def readfile(filename, columns, table1):
    with open(filename,'r') as f:

        for rivi in f:
            splitted=rivi.rstrip().partition('#')[0].split()
            if not splitted:
                continue
            if len(splitted)>0 and len(splitted)<=columns:
                table1.append(splitted)
            else:
                raise ValueError("Something wrong with the number of columns in the file.")

def calculate_volume(table1, table2, volpros): #total volume of proteins and water
    a=1
    volume_tot=0.0
    for i in table2:
        if i[0]==table1[a][0]:
            volume_protein=int(table1[a][1])*float(i[1])
            volume_tot=volume_tot+volume_protein
            a+=1
            if a==(len(table1)):
                break
    volume = volume_tot/volpros
    return volume

def calculate_tolerance(bigger_volume, table1): #calculates the largest tolerance for proteins for packmol
    number=0
    i=1
    while i<len(table1):
       number+=float(table1[i][1])
       i+=1
    volume_per_protein=bigger_volume/number
    tolerance=(volume_per_protein**(1/3))*0.23

    return tolerance

def calculate_waters(volume, volpros): #number of waters in the system
    volpros_w=1.0-volpros
    waters=int((volpros_w*volume)/W_VOLUME)
    return waters

def calculate_ions(waters):
    ions=(waters*4)/380.37
    return ions

def create_packmol(table1, box_size_an, waters, tolerance): #creates packmol input file
    packmol_inp="0-proteins.inp"
    packmol_out="0-proteins.pdb"
    with open(packmol_inp,'w') as f:  #first only proteins to packmol
        f.write("tolerance {:.1f}\n\n".format(tolerance))
        f.write("filetype pdb\n\n")
        f.write("output {0}\n\n".format(packmol_out))
        f.write("nloop 10000\n\n")
        i=1
        while i<len(table1):
            f.write("structure ./topology/{0}_cg.pdb\n".format(table1[i][0]))
            f.write("  number {0}\n".format(table1[i][1]))
            f.write("  inside box 0. 0. 0. {0}. {0}. {0}.\n".format(box_size_an))
            f.write("end structure\n\n")
            i+=1

    packmol_water_inp="2-proteins_water.inp"
    packmol_water_out="2-proteins_water.pdb"
    structure="1-proteins_min.pdb"
    with open(packmol_water_inp, 'w') as x:  #second adding the waters to fixed proteins with packmol
        x.write("tolerance {:.1f}\n\n".format(4.5))
        x.write("filetype pdb\n\n")
        x.write("output {0}\n\n".format(packmol_water_out))
        x.write("nloop 10000\n\n")
        x.write("structure {0}\n".format(structure))
        x.write("  number 1\n")
        x.write("  fixed 0. 0. 0. 0. 0. 0.\n")
        x.write("end structure\n\n")
        x.write("structure ./topology/water_cg.pdb\n")
        x.write("  number {0}\n".format(waters))
        x.write("  inside box 0. 0. 0. {0}. {0}. {0}.\n".format(box_size_an))
        x.write("end structure\n")

def calculate_charge(table1, table2):
    a=1
    sum=0
    for i in table2:
        if i[0]==table1[a][0]:
            charge=int(i[2])*int(table1[a][1]) # charge of the protein multiplied by the amount
            sum=sum+charge # if multiple proteins in the in put file with different charges
            a+=1
            if a==(len(table1)):
                break
    return sum

def create_topology(topology, table1, waters, na, cl, waters2, WFs, system, forcefield, reducedto): #creates topology file "topol.top"
    with open(topology, 'w') as f:
        if forcefield=="martini22":
            itp="martini_v2.2"
        if forcefield=="martini21":
            itp="martini_v2.1"
        f.write("#include \"./topology/{:}\" \n \n".format(itp+'.itp'))
        itp_protein=itp + "_proteinff" + str(reducedto) + '.itp'
        f.write("#include \"./topology/{:}\" \n \n".format(itp_protein))
        f.write("#include \"./topology/{:}\" \n \n".format(itp+'_water.itp'))
        f.write("#include \"./topology/martini_v2.0_ions.itp\"\n\n")
        f.write("#define RUBBER_BANDS \n\n")
        i=1
        while i<len(table1):
            f.write("#include \"./topology/{0}_cg.itp\"\n".format(table1[i][0]))
            i+=1
        f.write("\n")
        f.write("[ system ]\n")
        f.write("; name\n")
        f.write("Martini system proteins in aquaeous solution\n\n")
        f.write("[ molecules ]\n")
        f.write("; name        number\n")
        i=1
        while i<len(table1):
            f.write("{0}        {1}\n".format(table1[i][0], table1[i][1]))
            i+=1
        if system==True:
            f.write("NA+           {:.0f}\n".format(na))
            f.write("CL-           {:.0f}\n".format(cl))
            f.write("W             {0}\n".format(waters2))
            f.write("WF            {0}\n".format(WFs))

def create_vmd(table1, table2, sort):
    with open("vmd_input.script", 'w') as f:
        #f.write("menu main on\n\n")
        #f.write("menu files on\n\n")
        f.write("mol new {4-proteins_water_comp.gro} type {gro} first 0 last -1 step 1 waitfor 1\n")
        f.write("animate style Loop\n")
        f.write("mol addfile {proteins_water_sim.xtc} type {xtc} first 0 last -1 step 1 waitfor -1 0\n")
        f.write("animate style Loop\n\n")
        #f.write("menu files off\n\n")
        f.write("mol delrep 0 0\n\n")
        i=1
        beads=0
        colour=0
        
        while i<len(table1):
            name=table1[i][0]  #protein and how many of them
            number=int(table1[i][1])

            for a in table2:  #search where the protein is in database
                if a[0]==name:
                    while number!=0: #different colours for all proteins
                        if sort==False: #all the proteins in different colours
                            f.write("mol color ColorID {0}\n".format(colour))
                            colour+=1
                            if colour==33:
                                colour=0
                            f.write("mol representation Points 10\n")
                            previous_beads=beads
                            beads+=int(a[3])
                            f.write("mol selection serial {0} to {1}\n".format(previous_beads+1, beads))
                            number-=1
                        if sort==True: #all same types of proteins in same colour
                            f.write("mol color ColorID {0}\n".format(colour))
                            f.write("mol representation Points 10\n")
                            previous_beads=beads
                            beads+=int(a[3])
                            f.write("mol selection serial {0} to {1}\n".format(previous_beads+1, beads))
                            number-=1
                            if number==0:
                                colour+=1
                            
                        f.write("mol material Opaque\n")
                        f.write("mol addrep 0\n\n")
            i+=1



if __name__ == '__main__':

    (options, args) = optP()

    inputfile=options.inputfile
    datainput=options.datainput
    multiply=options.multiply
    sort=options.verbose
    forcefield=options.forcefield
    reducedto=options.reducedto

    input_table1=[]
    readfile(inputfile,2,input_table1) #read both input and protein database files
    database_table=[]
    readfile(datainput,5,database_table)

    volpros=float(input_table1[0][0])/100.0 #the volume prosentage of proteins in the system devided by 100

    volume=calculate_volume(input_table1,database_table, volpros)
    bigger_volume=multiply*volume  #calculate volume for packmol box
    tolerance=calculate_tolerance(bigger_volume,input_table1)*10 #tolerance in Ångströms
    waters=calculate_waters(volume, volpros) #calculate the number of waters=((1-volpros)*Vtot)/V(one water bead)
    box_size_an=int((bigger_volume**(1/3))*10) #packmol box size in Ångström
    with open("box.txt", 'w') as f:  #write a file that contains the info of a larger box size that will be the size of pbc box
        pbc_box=int((1.03*box_size_an)/10)
        f.write("{0}".format(pbc_box))

    print("Volume: {:.2f}".format(volume))
    print("Volume(larger): {:.2f}".format(bigger_volume))
    print("Number of waters: {0}".format(waters))
    print("Box side size: {0}".format(box_size_an))
    print("Tolerance for proteins: {:.1f}".format(tolerance))

    create_packmol(input_table1, box_size_an, waters, tolerance)

    charge=calculate_charge(input_table1, database_table)
    ions=int(calculate_ions(waters))
    na=ions
    cl=ions
    if charge < 0:
        na+=abs(charge)
    else:
        cl+=charge
    waters2=int(0.9*(waters-cl-na)) #waters remaining after ions and 10% WF
    WFs=int((waters-cl-na)-waters2) #10% WFs

    create_topology("topol_prot.top", input_table1, waters, na, cl, waters2, WFs, False, forcefield, reducedto)
    create_topology("topol.top", input_table1, waters, na, cl, waters2, WFs, True, forcefield, reducedto) #topology for the system with proteins and water and ions
    create_vmd(input_table1, database_table, sort)
