#!/usr/bin/env python3

# To join MARTINIZED pdb files

from optparse import OptionParser



def optP():

    parser = OptionParser()
    parser.add_option('-o', '--output', type='string',
                        action='store', dest='new_file', default='test.pdb')
    return parser.parse_args()


if __name__ == '__main__':
    
    (options, args) = optP()
    files_list=[]
    
    # this part just makes life easier as it can identify it's inputs from a database and you don't need a max amount of inputs in MARTINIZEd

    with open('../pdb_database.dat', 'r') as database:
       for line in database:
            splitted=line.strip().partition('#')[0].split()
            if not splitted:
                continue
            else:
                i=0
                nocs=int(splitted[2])
                while i < nocs:
                    filename='6-' + splitted[0] + '_' + str(nocs) + splitted[i+3] + '_cg_martinize.pdb'
                    files_list.append(filename)
                    i+=1
    last_number=0
    for i in files_list:
        with open(i, 'r') as read:
            with open (options.new_file, 'a') as new:
                for nline, line in enumerate(read):
                    rivi=line.split()
                    if nline<3:
                        continue  
                    if len(rivi)==11 and rivi[0]=="ATOM":
                        rivi[1]=last_number+1
                        last_number+=1
                        new.write("{:}{:>7}{:>5}{:>4}{:>2}{:>4}{:>12}{:>8}{:>8}{:>6}{:>6}\n".format(rivi[0],rivi[1],rivi[2],rivi[3],rivi[4],rivi[5],rivi[6],rivi[7],rivi[8],rivi[9],rivi[10]))
                    if len(rivi)==10:
                        rivi[1]=last_number+1
                        last_number+=1
                        new.write("{:}{:>7}{:>5}{:>4}{:>6}{:>12}{:>8}{:>8}{:>6}{:>6}\n".format(rivi[0],rivi[1],rivi[2],rivi[3],rivi[4],rivi[5],rivi[6],rivi[7],rivi[8],rivi[9]))
    with open (options.new_file, 'a') as f:
        f.write("TER\n")
        f.write("ENDML\n")
        
            
                
            
        