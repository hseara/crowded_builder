from optparse import OptionParser
import sys
import os

if __name__ == "__main__":
    #sys.argv = ['MSEtoMET.py', '-i', '0-1YBY.pdb', '-o', 'test.pdb']
    print (sys.argv[0:])
    
    parser = OptionParser()
    parser.add_option("-i", "--filein", action="store", type="string", dest="fni")
    parser.add_option("-o", "--fileout", action="store", type="string", dest="fno")
    (options, args) = parser.parse_args() 
    print (options)
    print (args)
    
    #Read input pdb file by lines
    __location__ = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    
    infile = open(os.path.join(__location__, options.fni), 'r')
    outfile = open(os.path.join(__location__, options.fno), 'w')
    
    for line in infile:
        #Transform MSE (Selenomethionine) to MET (Methionine) this HETATM
        if line.startswith('HETATM', 0, 6) :
            if 'MSE' in line:
                line=line.replace('HETATM', 'ATOM  ').replace('MSE', 'MET').replace(' SE',' SD')
                outfile.write(line)
            else:
                continue
            
        else:
            outfile.write(line)
        # Remove lines with HETATM
    