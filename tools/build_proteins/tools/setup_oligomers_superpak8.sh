#!/bin/bash

export PATH=$PATH:/usr/local/amber/amber12/bin
export AMBERHOME=/usr/local/amber/amber12

# I know it's stupid but you need a lot of python programs to do this. 1) MSEtoMET.py 2) select_chain.py 3) martinize.py 4) joi# n_pdbs.py


while read i; do
  line=( $i )

# Name of the protein and number of chains

  name=${line[1]}
  nocs=${line[2]}


  if [[ "${line[0]}" == "#" ]]; then
   continue
  fi
 
  cd ./${name}
  
# Clean

  cp -f ../MSEtoMET.py .
  python MSEtoMET.py -i 0-${line[0]}_superpak8.pdb -o 1-${line[0]}_clean.pdb

# Continue to operate with seperate chains

  i='0'
  
  while (( $i < $((nocs)) )); do

    ../select_chains.py -i 1-${line[0]}_clean.pdb -c ${line[$((3+i))]} -o  2-${line[0]}_${nocs}${line[$((3+i))]}.pdb --super
	
    echo "source leaprc.ff12SB" >3-my_tleap_script$i
    echo "x = loadpdb 2-${line[0]}_${nocs}${line[$((3+i))]}.pdb" >>3-my_tleap_script$i 
    echo "savepdb x 3-${line[0]}_${nocs}${line[$((3+i))]}_tleap.pdb" >>3-my_tleap_script$i
    echo "quit" >>3-my_tleap_script$i

    tleap -f 3-my_tleap_script$i

    editconf -f 3-${line[0]}_${nocs}${line[$((3+i))]}_tleap.pdb -o 4-${line[0]}_${nocs}${line[$((3+i))]}_label.pdb -label ${line[$((3+i))]}

    sed -e "s/HIE/HIS/" -e "s/HID/HIS/" -e "s/HIP/HIH/" < 4-${line[0]}_${nocs}${line[$((3+i))]}_label.pdb > 5-${line[0]}_${nocs}${line[$((3+i))]}_HIS.pdb

    mkdssp -i ./5-${line[0]}_${nocs}${line[$((3+i))]}_HIS.pdb -o ./5-${line[0]}_${nocs}${line[$((3+i))]}.dssp

    
    python2 ../martinize.py -f 5-${line[0]}_${nocs}${line[$((3+i))]}_HIS.pdb \
                            -ss 5-${line[0]}_${nocs}${line[$((3+i))]}.dssp \
                            -o ${line[0]}_${nocs}${line[$((3+i))]}_cg.top \
                            -x 6-${line[0]}_${nocs}${line[$((3+i))]}_cg_martinize.pdb \
                            -n ${line[0]}_${nocs}${line[$((3+i))]}_cg.ndx \
                            -p all \
                            -cys auto\
                            -ff martini21 -elastic

    mv ./Protein_${line[$((3+i))]}.itp ./${line[0]}_${nocs}${line[$((3+i))]}_cg.itp
    
    sed -e "s/Protein_${line[$((i+3))]}/${line[0]}_${nocs}${line[$((3+i))]}/" -i ${line[0]}_${nocs}${line[$((3+i))]}_cg.itp

        
    i=$[$i+1]

 done

# Join martinized models together in one pdb and create a single topology  

  ../join_pdbs_cg.py -o 6-${name}_cg_martinize.pdb

  echo "#include \"martini_v2.1.itp\"" >> ${name}_cg.top
  echo "#define RUBBER_BANDS" >> ${name}_cg.top

  i='0'

  while (( $i < $((nocs)) )); do
  
    echo "#include \"${line[0]}_${nocs}${line[$((3+i))]}_cg.itp\"" >> ${name}_cg.top

     i=$[$i+1]
 
  done 

  echo "" >> ${name}_cg.top
  echo "[ system ]" >> ${name}_cg.top
  echo "Martini system from ${name}_HIS.pdb" >> ${name}_cg.top
  echo "" >> ${name}_cg.top
  echo "[ molecules ]" >> ${name}_cg.top

  i='0'
  
  while (($i < $((nocs)) )); do

    echo "${line[0]}_${nocs}${line[$((3+i))]}       1" >> ${name}_cg.top

    i=$[$i+1]

  done


  # Minimization

   editconf -f  6-${name}_cg_martinize.pdb \
           -o  7-${name}_cg_box.gro \
           -box 30 30 30 \
           -center 15 15 15

  cp ../martini_v2.1.itp .
  cp ../min.mdp ./8-min.mdp

  grompp -f 8-min.mdp \
         -c 7-${name}_cg_box.gro \
         -p ${name}_cg.top \
         -o 8-${name}_cg_min.tpr \
         -po 8-${name}_cg_min-mdout.tpr
 
  mdrun_d -v -nt 1 -deffnm 8-${name}_cg_min

  # Small equilibration

  cp ../md_berendsen_1fs.mdp  9-md_berendsen_1fs.mdp

  grompp -f 9-md_berendsen_1fs.mdp \
         -c 8-${name}_cg_min.gro \
         -p ${name}_cg.top \
         -o 9-${name}_cg_md.tpr \
         -po 9-${name}_cg_md-mdout.tpr

  mdrun_d -v -deffnm 9-${name}_cg_md

  editconf -f 9-${name}_cg_md.gro -o ${name}_cg.pdb

  mkdir build; mv ?-* ./build
 
  rm -f ./#*

  cd ..

done < pdb_database.dat
