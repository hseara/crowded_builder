#!/usr/bin/env python3

# selects and separates chains from PDB. 

import re
from optparse import OptionParser

def optP():

    parser = OptionParser()
    parser.add_option('-i', '--input',
                    type='string', action='store',
                    dest='input')
    parser.add_option('-o', '--output', type='string',
                        action='store', dest='newfile')
    parser.add_option('-c', '--chain', type='string', dest='chain')
    parser.add_option('--super', action="store_true", dest="verbose", default=False)
    return parser.parse_args()
 

def select_chain(input, chain, newfile):
 
    with open(input, 'r') as file:
        with open(newfile, 'w') as new:
            for line in file:
                if options.verbose==True:  # if the purpose is to select from superpak8-output
                    pattern="[0-9,a-z,A-Z, ]{20}" + chain
                else:
                    pattern=r"^ATOM[0-9,a-z,A-Z, ]{17}["+ chain +"]"
                    
                    
                if re.match(pattern, line):
                    new.write("{:}".format(line))
            return
                
            
if __name__ == '__main__':
    
    (options, args)=optP()
    
    input=options.input
    chain=options.chain
    newfile=options.newfile
    
    select_chain(input, chain, newfile)
    
    
    

            
            