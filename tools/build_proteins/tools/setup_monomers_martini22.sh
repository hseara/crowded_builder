#!/bin/bash


# This program creates protein monomers using martini force field from given inputfile to a wanted destination.
# You call this program by "setup_monomers.sh -i inputfile -d directory -f force field (=martini22, martini21)
# Notice that you need to give the whole paths !



while getopts ":hf:i:d:" opt; do
  case $opt in
    h)
      echo "-i inputfile -f martini22/martini21 -d directory" >&2
      exit 1
      ;;
    f)
      forcefield=$OPTARG
       if [[ "${forcefield}" == "martini22" ]]; then
        itp="martini_v2.2.itp"
        echo "itp-file $itp" >&2
       fi
       if [[ "${forcefield}" == "martini21" ]]; then
        itp="martini_v2.1.itp"
       fi
      ;;
    i)
      inputfile=$OPTARG
      echo "option -i triggered, argument $OPTARG" >&2
      ;;
    d)
      folder=$OPTARG
      echo "option -d triggered, argument $OPTARG" >&2
      ;;
    /?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;
    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done



FIRST=`pwd`


export PATH=$PATH:/usr/local/amber/amber12/bin
export AMBERHOME=/usr/local/amber/amber12


while read i; do

  line=( $i )

  if [[ "${line[0]}" == "#" ]]; then
   continue
  fi

# Copy everything needed to the wanted destination

  mkdir ${folder}/${line[0]}_1${line[1]}
  cp ../../../data/ff_itp/${itp} ${folder}/${line[0]}_1${line[1]}
  cp MSEtoMET.py ${folder}/${line[0]}_1${line[1]}
  cp ../../../data/mdp/min.mdp ${folder}/${line[0]}_1${line[1]}/8-min.mdp
  cp ../../../data/mdp/md_berendsen_1fs.mdp  ${folder}/${line[0]}_1${line[1]}/9-md_berendsen_1fs.mdp
  cp martinize.py ${folder}/${line[0]}_1${line[1]}

  cd ${folder}/${line[0]}_1${line[1]}

  wget -c "http://www.pdb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId="${line[0]} -O ./0-${line[0]}.pdb

  #Clean non protein fragments and convert MSE syntetic aminoacids to MET
  python MSEtoMET.py -i ./0-${line[0]}.pdb -o ./1-${line[0]}_clean.pdb
  sed '/^HETATM/d' ./0-${line[0]}.pdb > ./1-${line[0]}_clean.pdb

  #Select chain
  grep ATOM ./1-${line[0]}_clean.pdb | grep " ${line[1]} " > ./2-${line[0]}_1${line[1]}_raw.pdb

  #Fix structure to account for missing atoms
  echo "source leaprc.ff12SB" >3-my_tleap_script 
  echo "x = loadpdb 2-${line[0]}_1${line[1]}_raw.pdb" >>3-my_tleap_script 
  echo "savepdb x 3-${line[0]}_1${line[1]}_tleap.pdb" >>3-my_tleap_script 
  echo "quit" >>3-my_tleap_script 
  tleap -f 3-my_tleap_script
  mv leap.log 3-leap.log

  #Addchain label back
  editconf -f 3-${line[0]}_1${line[1]}_tleap.pdb -o 4-${line[0]}_1${line[1]}_label.pdb -label ${line[1]}

  #Renumber as in the original pdb. Tleap changes the order


  #Change HIS to match martinize requirements
  sed -e "s/HIE/HIS/" -e "s/HID/HIS/" -e "s/HIP/HIH/" < 4-${line[0]}_1${line[1]}_label.pdb > 5-${line[0]}_1${line[1]}_HIS.pdb

  #Generate secondary structure file
  mkdssp -i ./5-${line[0]}_1${line[1]}_HIS.pdb -o ./5-${line[0]}_1${line[1]}.dssp

  #Martinize structure
  python2 martinize.py -f 5-${line[0]}_1${line[1]}_HIS.pdb \
                          -ss 5-${line[0]}_1${line[1]}.dssp \
                          -o ${line[0]}_1${line[1]}_cg.top \
                          -x 6-${line[0]}_1${line[1]}_cg_martinize.pdb \
                          -n ${line[0]}_1${line[1]}_cg.ndx \
                          -p all \
                          -cys auto\
                          -ff $forcefield -elastic
  ##merge option for multiple chains in one molecule##
  # What is needed
  mv ./Protein_${line[1]}.itp ./${line[0]}_1${line[1]}_cg.itp
  sed -e "s/Protein_${line[1]}/${line[0]}_1${line[1]}/" -i ${line[0]}_1${line[1]}_cg.itp
  # Top file, not very useful
  sed -e "s/martini.itp/${itp}/" -i ./${line[0]}_1${line[1]}_cg.top
  sed -e "s/Protein_${line[1]}.itp/${line[0]}_1${line[1]}_cg.itp/" -i ./${line[0]}_1${line[1]}_cg.top
  sed -e "s/Protein_${line[1]}/${line[0]}_1${line[1]}/" -i ./${line[0]}_1${line[1]}_cg.top

  #Minimization protein
  editconf -f  6-${line[0]}_1${line[1]}_cg_martinize.pdb \
           -o  7-${line[0]}_1${line[1]}_cg_box.gro \
           -box 30 30 30 \
           -center 15 15 15

  grompp -f 8-min.mdp \
         -c 7-${line[0]}_1${line[1]}_cg_box.gro \
         -p ${line[0]}_1${line[1]}_cg.top \
         -o 8-${line[0]}_1${line[1]}_cg_min.tpr \
         -po 8-${line[0]}_1${line[1]}_cg_min-mdout.tpr
 
  mdrun_d -v -deffnm  8-${line[0]}_1${line[1]}_cg_min

  #Small equilibration
   grompp -f 9-md_berendsen_1fs.mdp \
         -c 8-${line[0]}_1${line[1]}_cg_min.gro \
         -p ${line[0]}_1${line[1]}_cg.top \
         -o 9-${line[0]}_1${line[1]}_cg_md.tpr \
         -po 9-${line[0]}_1${line[1]}_cg_md-mdout.tpr
 
  mdrun_d -v -deffnm  9-${line[0]}_1${line[1]}_cg_md

  # generate final pdb
  editconf -f 9-${line[0]}_1${line[1]}_cg_md.gro -o  ${line[0]}_1${line[1]}_cg.pdb

  #Move all atom files to build directory
  mkdir build; mv ?-* ./build

  cd $FIRST

done < $inputfile
