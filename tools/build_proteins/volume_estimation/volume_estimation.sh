#!/bin/bash

NameOutDir="volume_estimation"
sim=6 #number of simulatione
MDP=('md_berendsen_2fs_P50' 'md_verlet12_berendsen_10fs' 'md_berendsen_10fs' 'md_parrinello_rahman_10fs' 'md_verlet11_berendsen_10fs' 'md_parrinello_rahman_15fs')

mkdir $NameOutDir

while read i; do
  line=( $i )

  if [[ "${line[0]}" == "#" ]]; then
   continue
  fi

  cd ./${line[0]}_m${line[1]}
  rm -rf $NameOutDir
  mkdir $NameOutDir; cd $NameOutDir

  #Copy forcefield fields
  cp ../${line[0]}_m${line[1]}_cg.itp .
  cp ../../martini_v2.1.itp .

  #Copy structure files
  cp ../${line[0]}_m${line[1]}_cg.pdb ./0-${line[0]}_m${line[1]}_cg.pdb
  cp ../../water/waterbox400_cg.gro .

  #Add very large box
  editconf -f 0-${line[0]}_m${line[1]}_cg.pdb -o 1-${line[0]}_m${line[1]}_cg_box.gro -box 20 20 20


  #Solvate the system
  cp ../${line[0]}_m${line[1]}_cg.top 1-${line[0]}_m${line[1]}_cg_sol.top
  genbox -cp 1-${line[0]}_m${line[1]}_cg_box.gro -cs waterbox400_cg.gro -o 2-${line[0]}_m${line[1]}_cg_sol.gro -maxsol 20000 -vdwd 0.21
  cp 1-${line[0]}_m${line[1]}_cg_sol.top 2-${line[0]}_m${line[1]}_cg_sol.top
  echo "" >> 2-${line[0]}_m${line[1]}_cg_sol.top
  for i in {1..20}; do
    echo "W                  900" >> 2-${line[0]}_m${line[1]}_cg_sol.top
    echo "WF                 100" >> 2-${line[0]}_m${line[1]}_cg_sol.top
  done

  #Equilibration => Qucik compression
  cp ../../${MDP[0]}.mdp ./3-${MDP[0]}.mdp
  grompp -f 3-${MDP[0]}.mdp -maxwarn 1 \
         -c 2-${line[0]}_m${line[1]}_cg_sol.gro \
         -p 2-${line[0]}_m${line[1]}_cg_sol.top \
         -o 3-${line[0]}_m${line[1]}_cg_sol-${MDP[0]}.tpr \
         -po 3-${line[0]}_m${line[1]}_cg_sol-${MDP[0]}-mdout.mdp
  mdrun -v -rdd 2.0 -deffnm 3-${line[0]}_m${line[1]}_cg_sol-${MDP[0]} -testverlet

  #Simulations:
  np=3
  nc=4
  for i in {1..5}
  do
   
    cp ../../${MDP[$i]}.mdp ./$nc-${MDP[$i]}.mdp
    grompp -f $nc-${MDP[$i]}.mdp \
           -c $np-${line[0]}_m${line[1]}_cg_sol-${MDP[$((i-1))]}.gro \
           -p 2-${line[0]}_m${line[1]}_cg_sol.top \
           -o $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}.tpr \
           -po $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}-mdout.mdp
    mdrun -v -rdd 2.0 -deffnm $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}
    echo "Volume"|g_energy -f $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}.edr \
                           -o $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}-volume.xvg
    cp -f $nc-${line[0]}_m${line[1]}_cg_sol-${MDP[$i]}-volume.xvg ../../$NameOutDir/

    np=$((np+1))
    nc=$((nc+1))
  done
 
  cd ../../

done < proteins_monomers.dat
