#!/bin/bash

NameOutDir="volume_estimation_water"
sim=6 #number of simulations
MDP=('md_berendsen_1fs' 'md_verlet12_berendsen_10fs' 'md_berendsen_10fs' 'md_parrinello_rahman_10fs' 'md_verlet11_berendsen_10fs' 'md_parrinello_rahman_15fs')

mkdir $NameOutDir

cd volume_estimation_water
rm -rf *

#Copy forcefield fields
cp ../martini_v2.1.itp .

#Copy structure files
cp ../water/waterbox400_cg.gro ./

#Create the water simulation box
genbox -cs waterbox400_cg.gro -o 1-waterbox_10000_cg.gro -maxsol 10000 -vdwd 0.21 -box 15 15 15

#create top file
echo '#include "martini_v2.1.itp"' > 1-waterbox_10000_cg.top 
echo "" >> 1-waterbox_10000_cg.top
echo "[ system ]" >> 1-waterbox_10000_cg.top
echo "; name" >> 1-waterbox_10000_cg.top
echo "water box coarse grained (martini)" >> 1-waterbox_10000_cg.top
echo "" >> 1-waterbox_10000_cg.top
echo "[ molecules ]" >> 1-waterbox_10000_cg.top
echo "; name    number" >> 1-waterbox_10000_cg.top

for i in {0..9}; do
  echo "W          900" >> 1-waterbox_10000_cg.top
  echo "WF         100" >> 1-waterbox_10000_cg.top
done

#Equilibration
cp ../${MDP[0]}.mdp ./3-${MDP[0]}.mdp
grompp -f 3-${MDP[0]}.mdp -maxwarn 1 \
       -c 1-waterbox_10000_cg.gro \
       -p 1-waterbox_10000_cg.top \
       -o 3-waterbox_10000_cg-${MDP[0]}.tpr \
       -po 3-waterbox_10000_cg-${MDP[0]}-mdout.mdp
mdrun -v -rdd 2.0 -deffnm 3-waterbox_10000_cg-${MDP[0]} -testverlet

#Simulations:
np=3
nc=4
for i in {1..5}; do
  cp ../${MDP[$i]}.mdp ./$nc-${MDP[$i]}.mdp
  grompp -f $nc-${MDP[$i]}.mdp \
         -c $np-waterbox_10000_cg-${MDP[$((i-1))]}.gro \
         -p 1-waterbox_10000_cg.top \
         -o $nc-waterbox_10000_cg-${MDP[$i]}.tpr \
         -po $nc-waterbox_10000_cg-${MDP[$i]}-mdout.mdp
  mdrun -v -rdd 2.0 -deffnm $nc-waterbox_10000_cg-${MDP[$i]}
  echo "Volume"|g_energy -f $nc-waterbox_10000_cg-${MDP[$i]}.edr \
                           -o $nc-waterbox_10000_cg-${MDP[$i]}-volume.xvg
  np=$((np+1))
  nc=$((nc+1))
done
 
