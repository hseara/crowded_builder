#!/bin/bash

while getopts ":hf:i:d:p:" opt; do
  case $opt in
    h)
      echo "-i inputfile -f martini22/martini21 -d directory -p prosentage" >&2
      exit 1
      ;;
    i)
      input=$OPTARG
      echo "option -i triggered, argument $OPTARG" >&2
      ;;

    f)
      forcefield=$OPTARG
       if [[ "${forcefield}" == "martini22" ]]; then
        itp="martini_v2.2"
        echo "itp-file $itp" >&2
       fi
       if [[ "${forcefield}" == "martini21" ]]; then
        itp="martini_v2.1"
       fi
      ;;

    d)
      dir=$OPTARG
      echo "option -d triggered, argument $OPTARG" >&2
      ;;

    p)
      reducedto=$OPTARG
      echo "option -p triggered, argument $OPTARG" >&2
      ;;

    /?)
      echo "Invalid option: -$OPTARG" >&2
      exit 1
      ;;

    :)
      echo "Option -$OPTARG requires an argument." >&2
      exit 1
      ;;
  esac
done


FIRST=`pwd`

a="0"


cd $dir

mkdir build
cd build

# Copy mdps

cp -f $FIRST/data/mdp/* .

mkdir topology
cd topology

while read i; do 

  line=( $i )
   
  [ -z "${line}" ] && continue

  if [[ "${line[0]}" == "#" ]]; then
   continue
  fi

  if (( "${a}" == "0" )); then
    a=$[$a+1]
    continue
  fi

# Copies the structures for the proteins, itp, pdb, top.

  cp -f $FIRST/data/proteins/${forcefield}/${line[0]}/${line[0]}_cg.pdb .
  cp -f $FIRST/data/proteins/${forcefield}/${line[0]}/${line[0]}_cg.itp .

# Protein force field: everything in protein.itp appended with "p"

  $FIRST/tools/proteinitp.py --itp ${line[0]}_cg.itp --oitp ${line[0]}_cg_p.itp
   mv -f ${line[0]}_cg_p.itp ${line[0]}_cg.itp
#  cp -f $FIRST/data/proteins/${forcefield}/${line[0]}/${line[0]}_cg.top .
 
done < $input

cp -f $FIRST/data/ff_itp/${itp}.itp .
cp -f $FIRST/data/ff_itp/${itp}_water.itp .
# cp -f $FIRST/data/ff_itp/${itp}_proteinff${reducedto}.itp .
cp -f $FIRST/data/ff_itp/${itp}_proteinff_elcock_A0.3.itp .
cp -f $FIRST/data/ff_itp/martini_v2.0_ions.itp .
cp -f $FIRST/data/water/water_cg.pdb .

cd ..

cp -f $FIRST/data/proteins/cg_protein_db.dat .  

$FIRST/tools/crowded_builder.py -f $input -i ${forcefield} -p ${reducedto}

read s < box.txt

ppackmol 4 0-proteins.inp

# Minimizing proteins in a bigger box

editconf -f 0-proteins.pdb -o 0-proteins.gro -box $s $s $s -noc

grompp -f min_500.mdp -c 0-proteins.gro -p topol_prot.top -o 1-proteins_min.tpr

mdrun_d -v -rdd 2.5 -deffnm 1-proteins_min
# Adding water

editconf -f 1-proteins_min.gro -o 1-proteins_min.pdb

ppackmol 4 2-proteins_water.inp

# Minimizing water and proteins

editconf -f 2-proteins_water.pdb -o 2-proteins_water.gro -noc -box $s $s $s

grompp -f min_100.mdp -c 2-proteins_water.gro -p topol.top -o 3-proteins_water_min.tpr -maxwarn 1

mdrun_d -v -rdd 2.5 -deffnm 3-proteins_water_min -nt 1

# Compressing

grompp -f md_berendsen_2fs_P50.mdp -c 3-proteins_water_min.gro -p topol.top -o 4-proteins_water_comp.tpr

mdrun -rdd 2.5 -v -deffnm 4-proteins_water_comp

# Done

cd .. 

cp ./build/4-proteins_water_comp.gro .

mv ./build/md_parrinello_rahman_10fs.mdp .
grompp -f md_parrinello_rahman_10fs.mdp -c 4-proteins_water_comp.gro -p ./build/topol.top -o proteins_water_sim.tpr

mdrun -v -rdd 2.5 -deffnm proteins_water_sim

# vmd -e ./build/vmd_input.script 


