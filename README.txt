Creating water-protein systems with create_system.sh:

-Needs an input file,example system_input1.dat, NOTICE THAT YOU HAVE TO GIVE THE WHOLE PATHS TO THE PROGRAM.

-Program call:

"./create_system.sh -d $FULLPATH/directory -i $FULLPATH/inputfile -f [martini22|martini21]"
(with -h you get the instructions)  

where directory is the directory what you want the simulation directory to be.

if there's errors, it is probably in the minimization of proteins with water. Change the amount of steps in min_100.mdp and minimize again.

not to commit
